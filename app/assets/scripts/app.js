import $ from 'jquery';
import "lazysizes";
import "../styles/styles.css";
import MobileMenu from "./modules/MobileMenu";
import RevealOnScroll from "./modules/RevealOnScroll";
import SmoothScroll from "./modules/SmoothScroll";
import ActiveLinks from "./modules/ActiveLinks";
import Modal from "./modules/Modal";
//Handles Mobile Menu/Header
let mobileMenu = new MobileMenu();


//Handles Reveal On Scroll
new RevealOnScroll($("#our-beginning"));
new RevealOnScroll($("#departments"));
new RevealOnScroll($("#counters"));
new RevealOnScroll($("#testimonials"));

//Adding smooth scroll functionality to our header links
new SmoothScroll();

//Adding active link status in our project
new ActiveLinks();

//handle Model 
new Modal();

 


if(module.hot){
    module.hot.accept();
}
//alert("Hello world From MedServices!");
console.log("This goes to console!");
console.log("Another Line");